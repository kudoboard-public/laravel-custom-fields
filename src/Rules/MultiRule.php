<?php namespace Givebutter\LaravelCustomFields\Rules;

use Givebutter\LaravelCustomFields\Models\CustomField;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Str;

class MultiRule implements Rule
{
    public function passes($attribute, $value):bool
    {
        $fieldId = (int) Str::after($attribute, 'field_');
        $customField =  CustomField::find($fieldId);
        $values = explode('<>',  $value);

        foreach ($values as $value) {
            if (!in_array($value, $customField->answers)) {
                return false;
            }
        }

        return true;
    }

    public function message():string
    {
        return 'The answer is not available';
    }
}